const name = "authorization";

// Note: Could be more advanced for real-life use-cases
const requestDetails = req => ({
  path: req.path,
  method: req.method,
  data: req.body,
});

const createAuthorizationMiddleware = ({ verifyUserPermissions }) => {
  const handler = ({ }) => ({ }) => async (req, res, next) => {
    // No authentication -> No authorization by default
    if (!req.user) return res.status(401).json({ message: "Not authenticated" });

    const result = await Promise.resolve(verifyUserPermissions(requestDetails(req), req.user));

    if (!result) return res.status(403).json({ message: "No permission to access the endpoint" });

    return next();
  };

  return {
    name,
    handler,
  }
};

module.exports = { createAuthorizationMiddleware };
