const { createFilesController } = require("./controller");
const { createFilesMiddleware } = require("./middleware");
const { createSignatureGenerator } = require("./signer");
const { createDownloader } = require("./download");

const createFiles = ({
  secret,
  paths: { static, dynamic },
  ttl,
  allowedExtensions,
  fileField,
}) => {
  const downloader = createDownloader({ static, dynamic });
  const signer = createSignatureGenerator({ secret, ttl, allowedExtensions });

  const fileController = createFilesController({ fileField }, { signer, downloader });
  const fileMiddleware = createFilesMiddleware({ path: dynamic, fileField }, { signer });

  return {
    controller: fileController,
    middleware: [fileMiddleware]
  };
};

module.exports = { createFiles };
