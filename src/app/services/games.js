const { games } = require("../../mafia-game");

const name = "games";

const gameFinished = () => result => games.gameFinished({ result });

module.exports = {
  name,
  functions: {
    gameFinished,
  }
};