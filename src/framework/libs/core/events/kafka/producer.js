// SEPARATE CLIENTS FOR PROEDUCING AND CONSUMING
const { KafkaClient, HighLevelProducer, KeyedMessage } = require("kafka-node");
const debug = require("debug");
// const crypto = require("crypto");
// Note: Use "HighLevelProducer" - Better default Paritioner (Round-Robin)
//  - Default "Producer", just sends ALL traffic to Partition 0 (NOT GOOD)

// DOES NOT WORK -> Use Round-Robin (NOT THAT BAD)
// const hashKeyPartitioner = (partitions, key) => {
//   console.log(partitions, key);
//   const buffer = crypto.createHash("sha256").update(key).digest();
//   const value = buffer.readIntBE(0, buffer.byteLength);
//   const index = value % partitions.length;
//   return partitions[index];
// };

const kafkaProducerLogger = debug("app").extend("system").extend("kafka").extend("producer");

const createProducer = ({ host, id }, topicMapper) => {
  let isReady = false;
  let queue = [];

  const kafkaClient = new KafkaClient({
    kafkaHost: host,
    connectRetryOptions: {
      retries: 3,
      maxTimeout: 1000,
      minTimeout: 1000
    },
    connectTimeout: 3000,
    requestTimeout: 2000
  });

  const producer = new HighLevelProducer(
    kafkaClient,
    {
      requireAcks: 1,
      ackTimeoutMs: 500,
      partitionerType: 2 // Using Round-Robin
      // partitionerType: 4 // Specifying that Custom Paritioner is used - DOES NOT WORK
    },
    // hashKeyPartitioner // DOES NOT WORK
  );

  const send = (type, data) => {
    if (!isReady) {
      kafkaProducerLogger("Producer is NOT ready - Queueing")

      queue.push({ type, data });

      return Promise.resolve();
    }

    return new Promise((res, rej) => {
      const topic = topicMapper(type);

      if (topic === null) return res();

      const message = new KeyedMessage(
        type,
        JSON.stringify({
          data,
          source: id, // Used for determining if the event is local
        })
      );

      kafkaProducerLogger("Sending message", type);

      // Important: Pass an ARRAY
      producer.send([
        {
          topic,
          messages: [message]
        }
      ], err => {
        if (err) return rej(err);

        return res();
      })
    });
  };

  producer.on("ready", async () => {
    isReady = true;

    kafkaProducerLogger("Producer is ready - Cleaning the queue");

    try {
      await Promise.all(queue.map(({ type, data }) => send(type, data)));

      kafkaProducerLogger("Queue is clean");

      queue.splice(0, queue.length); // Cleaning up waiting queue
    } catch (error) {
      kafkaProducerLogger("Could NOT clear queued messages", error);
    }
  });

  return { send }
};

module.exports = { createProducer };
