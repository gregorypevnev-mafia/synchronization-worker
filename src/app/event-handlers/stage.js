const type = "stage-started";

const handler = ({
  repositories: { games }
}) => async ({ gameId, stage, changes }) => {
  try {
    await games.updateDetails(gameId, {
      stage,
      players: changes || null,
    });
  } catch (e) {
    logger("Stage-Updated Error:", e);
  }
};

module.exports = {
  type,
  handler
}