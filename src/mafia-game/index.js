const games = require("./games");
const players = require("./players");
const roles = require("./roles");

module.exports = {
  games,
  players,
  roles,
};
