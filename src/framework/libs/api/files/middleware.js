const multer = require("multer");

const name = "file-upload";

const toFilename = ({ signature, extension }) => `${signature}-${Date.now()}.${extension}`;

const createFilesMiddleware = ({ path, fileField }, { signer }) => {
  const handler = ({ }) => ({ }) => {
    const storage = multer.diskStorage({
      destination: path,
      filename(req, file, cb) {
        try {
          const { signature, expires } = req.query;
          const user = req.token;
          const [filename, extension] = file.originalname.split(".");

          signer.verify(filename, extension, user, expires, signature);

          cb(null, toFilename({ signature, extension }));
        } catch (e) {
          cb(e);
        }
      },
    });

    // No need for filtering - everything is done implicitly in storage
    const upload = multer({ storage });

    return upload.single(fileField);
  }

  return {
    name,
    handler,
  }
}

module.exports = { createFilesMiddleware };
