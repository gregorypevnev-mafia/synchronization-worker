const crypto = require("crypto");

const hash = ({ filename, extension, user, secret, expires }) =>
  crypto.createHash("sha256")
    .update(filename)
    .update(extension)
    .update(user)
    .update(secret)
    .update(String(expires))
    .digest("hex")
    .toString();

module.exports = {
  hashFile: hash
};
