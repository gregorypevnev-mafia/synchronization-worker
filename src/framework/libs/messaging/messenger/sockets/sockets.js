const createSockets = ({
  onMessage,
  onClose
}) => {
  const sockets = {};

  const add = (socket, socketId, details) => {
    socket.id = socketId;
    socket.details = details;

    sockets[socketId] = socket;

    socket.on("message", message => {
      onMessage(JSON.parse(message), socket.id, socket.details);
    });

    socket.on("close", () => {
      remove(socketId);

      onClose(socket.id, socket.details);
    });
  };

  const remove = socketId => {
    if (!sockets[socketId]) return;

    sockets[socketId].close();

    delete sockets[socketId];
  };

  const modifyDetails = (socketId, details) => {
    const socket = sockets[socketId];

    if (socket) socket.details = { ...socket.details, ...details };
  };

  const changeUser = (socketId, user) => {
    modifyDetails(socketId, { user });
  };

  const changeRoom = (socketId, room) => {
    modifyDetails(socketId, { room });
  };

  const sendToSocket = (socketId, message) => new Promise((res, rej) => {
    const socket = sockets[socketId];

    if (!socket) return res();

    socket.send(JSON.stringify(message), {}, res);
  });

  const send = (socketIds, message) => Promise.all(
    socketIds.map(socketId => sendToSocket(socketId, message))
  );

  const broadcast = message => send(
    Array.from(Object.keys(sockets)),
    message,
  );

  return {
    add,
    remove,
    changeUser,
    changeRoom,
    send,
    broadcast,
  };
}

module.exports = { createSockets };
