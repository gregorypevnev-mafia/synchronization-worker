const type = "game-finished";

const handler = ({
  logger,
  repositories: { games: gamesRepo },
  services: { games: gamesService },
}) => async ({ gameId, result }) => {
  try {
    const finished = gamesService.gameFinished(result);

    await gamesRepo.updateGame(gameId, {
      ...finished,
      details: null,
    });
  } catch (e) {
    logger("Game-Finished Error:", e);
  }
};

module.exports = {
  type,
  handler
}