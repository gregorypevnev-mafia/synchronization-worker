const { PLAYER_ALIVE, PLAYER_DEAD, PLAYER_EXPOSED } = require("./common/constants");
const { phaseDetails } = require("./common/stages");
const { roleName, populateRoles } = require("./roles");
const { shuffleList } = require("./utils");

const PLAYER_STATUS_NAMES = {
  [PLAYER_ALIVE]: "Alive",
  [PLAYER_DEAD]: "Dead",
  [PLAYER_EXPOSED]: "Exposed",
};

const playerInfo = ({ id, details, role, status }) => ({
  id,
  role: roleName(role),
  status: String(PLAYER_STATUS_NAMES[status]),
  ...details,
});

const isPlayerActive = ({ phase }, { role, status }) => {
  if (status === PLAYER_DEAD) return false;

  const details = phaseDetails(phase);

  if (details.standby) return false;

  return details.role === null || details.role === role;
};

const initializePlayers = (players, roles) => {
  const rolesList = shuffleList(populateRoles(roles));

  return players.reduce((players, { id }, i) => ({
    ...players,
    [id]: {
      id,
      role: rolesList[i],
      status: PLAYER_ALIVE,
    }
  }));
};

const playersForPhase = (stage, players) =>
  Object.values(players).filter(player => isPlayerActive(stage, player));

module.exports = {
  playerInfo,
  isPlayerActive,
  initializePlayers,
  playersForPhase,
};
