const {
  KILL_ACTION,
  EXPOSE_ACTION,
  SAVE_ACTION,
  EXECUTE_ACTION,
  PLAYER_ALIVE,
  PLAYER_DEAD,
  PLAYER_EXPOSED
} = require("./constants");

const processActions = actions =>
  actions.reduce((result, { action, target }) => {
    switch (action) {
      case KILL_ACTION:
        return {
          ...result,
          [target]: PLAYER_DEAD,
        };
      case EXPOSE_ACTION:
        return {
          ...result,
          [target]: result[target] !== PLAYER_DEAD ? PLAYER_EXPOSED : result[target],
        }
      case SAVE_ACTION:
        return {
          ...result,
          [target]: result[target] === PLAYER_DEAD ? PLAYER_ALIVE : result[target],
        };
      case EXECUTE_ACTION:
        return {
          ...result,
          [target]: PLAYER_DEAD,
        };
      // Ignoring Skippable actions
      default:
        return result;
    }
  }, {});

module.exports = { processActions };
