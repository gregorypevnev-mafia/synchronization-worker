const name = "games";
const datasource = "database";
const transactional = false;

const GAMES_COLLECTION = "games";

const stageModification = stage => {
  if (!stage) return {};

  return { "details.stage": stage };
};

const playersModifications = players => {
  if (!players || players.length === 0) return {};

  return Object.keys(players).reduce(
    (modifications, playerId) => ({
      ...modifications,
      [`details.players.${playerId}.status`]: players[playerId],
    }),
    {}
  );
};

const modifications = (stage, players) => ({
  ...stageModification(stage),
  ...playersModifications(players),
});

const update = async (mongodb, gameId, modifications) => {
  const result = await mongodb.collection(GAMES_COLLECTION).updateOne(
    { _id: gameId },
    { $set: modifications },
    { upsert: false }
  );

  return result.modifiedCount === 1;
};

const updateGame = mongodb => async (gameId, modifications) => {
  const result = await update(mongodb, gameId, modifications);

  if (!result) throw new Error("Game not updated");
}

const updateDetails = mongodb => async (gameId, {
  stage,
  players,
} = {}) => {
  const result = await update(mongodb, gameId, modifications(stage, players));

  if (!result) throw new Error("Details not updated");
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    updateGame,
    updateDetails,
  },
};