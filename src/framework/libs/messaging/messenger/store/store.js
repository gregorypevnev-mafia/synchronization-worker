const debug = require("debug");
const { createDatabase } = require("./database");
const { createPublisher } = require("./publisher");
const { createSubscriber } = require("./subscriber");

const storeDebug = debug("app").extend("system").extend("messaging").extend("store");

const createStore = ({
  readClient,
  writeClient
}, {
  onMessage,
  onClose
}, messengerConfig) => {
  const database = createDatabase(writeClient, messengerConfig);
  const publisher = createPublisher(writeClient, messengerConfig);

  // Unicast
  const handleUnicast = async (type, payload, user) => {
    const userSocket = await database.findForUser(user);

    if (userSocket === null) return; // No Connection found

    onMessage({ type, payload, to: [userSocket] });

    storeDebug(`Unicast to User "${user}" (${userSocket}):`, type, payload);
  };

  // Multicast
  const handleMulticast = async (type, payload, users) => {
    const userSockets = await database.findForUsers(users);

    onMessage({ type, payload, to: userSockets });

    storeDebug(`Multicast to Users "${JSON.stringify(users)}" (${JSON.stringify(userSockets)}):`, type, payload);
  };

  // Broadcast
  const handleBroadcast = async (type, payload, room) => {
    const roomSockets = await database.findForRoom(room);

    if (roomSockets === null) return; // No Connections found

    onMessage({ type, payload, to: roomSockets });

    storeDebug(`Broadcast to Room "${room}" (${roomSockets}):`, type, payload);
  };

  // Anycast
  const handleAnycast = async (type, payload) => {
    onMessage({ type, payload, to: null });

    storeDebug(`Anycast:`, type, payload);
  };

  // Message is received from PubSub
  const messageReceived = ({ type, payload, ...info }) => {
    // Unicast
    if (info.user) return handleUnicast(type, payload, info.user);

    // Multicast
    if (info.users) return handleMulticast(type, payload, info.users);

    // Broadcast
    if (info.room) return handleBroadcast(type, payload, info.room);

    // Anycast
    return handleAnycast(type, payload);
  };

  const socketClosed = ({ socket }) => onClose(socket);

  const infoReceived = info => storeDebug(info);

  const subscriber = createSubscriber(readClient, {
    onMessage: messageReceived,
    onClose: socketClosed,
    onInfo: infoReceived,
  }, messengerConfig);

  const connect = async (socketId, userId, roomId) => {
    await database.save({ socketId, userId, roomId });
  };

  const changeRoom = async (socketId, roomId) => {
    console.log("CHANGING TO ROOM", roomId);
    await database.modify(socketId, { room: roomId });
  }

  const send = async message => {
    await publisher.message(message);
  };

  const disconnectSocket = async socketId => {
    publisher.close(socketId);

    await database.remove(socketId);
  };

  const disconnectUser = async userId => {
    const socketId = await database.findForUser(userId);

    if (!socketId) return null;

    await disconnectSocket(socketId);

    return socketId;
  };

  const initialize = async () => {
    subscriber.initialize();
  }

  return {
    connect,
    send,
    changeRoom,
    disconnectSocket,
    disconnectUser,
    initialize,
  };
};

module.exports = { createStore };
