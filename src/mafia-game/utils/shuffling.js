const shuffleList = originalList => {
  const list = originalList.slice(); // Avoiding in-place operations
  let subIndex, temp;

  for (let i = list.length - 1; i >= 1; i--) {
    subIndex = Math.floor(Math.random() * (i - 1));

    temp = list[subIndex];
    list[subIndex] = list[i];
    list[i] = temp;
  };

  return list;
};

module.exports = { shuffleList };
