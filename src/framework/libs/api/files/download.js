const fs = require("fs");
const pathUtil = require("path");

const checkFile = filename => new Promise((res, rej) => fs.exists(filename, res));

const findPath = async (paths, filename) => {
  const results = await Promise.all(paths.map(async path => {
    const filepath = pathUtil.join(path, filename);

    const exists = await checkFile(filepath);

    return exists ? filepath : null;
  }));

  return results.find(filepath => filepath !== null) || null;
};

const createStreamer = paths => async filename => {
  const filepath = await findPath(paths, filename);

  if (filepath === null) throw new Error("File not found");

  return fs.createReadStream(filepath);
};

const createDownloader = ({ static, dynamic }) => ({
  stream: createStreamer([dynamic, static]), // IMPORTANT: Providing in the exact order -> Checking for dynamic paths first
})

module.exports = { createDownloader };
